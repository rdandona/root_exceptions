#include <cassert>
#include <cstdio>
#include <exception>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <stdexcept>
#include <string>
#include <type_traits>

using namespace std;

FILE *pFile;
char buffer[100];
char *filename;

class RException : public std::runtime_error {
public:
 
   RException(const RException& exception)
        : std::runtime_error(exception.what()),
        exceptionId(exception.id()),
        innerException(NULL)
   {
        if (exception.inner() != NULL)
        {
            innerException = new RException(*exception.inner());
        }
        else
        {
            innerException = NULL;
        }
    } 
   RException(const std::string& _Message)
        : std::runtime_error(_Message),
            exceptionId(0),
        innerException(NULL)
        {}
    
   RException(const std::string& _Message, unsigned int id, RException* innerException)
        : std::runtime_error(_Message),
        exceptionId(id),
        innerException(new RException(*innerException)){}

    virtual ~RException()
    {
        delete innerException;
    }

    unsigned int id() const { return exceptionId; }
    const RException* inner() const { return innerException; }
   
private:
    unsigned int exceptionId;
    const RException* innerException;
};
  
void handleException(RException *ex)
{
    cout << "exception " << ex->id() << " - " << ex->what() << endl;
        const RException* innerException = ex->inner();
    int t = 1;
    while (innerException != NULL)
    {
        for (int i=0; i<t; ++i)
        {
            cout << "\t";
        }
        ++t;
        cout << "inner exception " << innerException->id() << " - " << innerException->what() << endl;
        innerException = innerException->inner();
    }
}

template <typename T>
class RStatus {
public:
   RStatus() : fValue(), fErrorState(), fIsChecked(false) { GuessErrorState(); }
   explicit RStatus(const T &value) : fValue(value), fErrorState(), fIsChecked(false) { GuessErrorState(); }
   ~RStatus() noexcept(false)
   {
      if (!fIsChecked && IsError()) {
         if (!std::uncaught_exceptions())
            throw RException("return value not checked");
      }
   }

   operator const T&() {
      fIsChecked = true;
      return fValue;
   }

   void SetErrorState(const T &errorState) { fErrorState = errorState; }

   void* operator new(size_t size) = delete;

private:
   void GuessErrorState() {
      if constexpr(std::is_signed<T>::value)
         fErrorState = -1;
      else if constexpr(std::is_pointer<T>::value)
         fErrorState = nullptr;
   }

   bool IsError() const { return fValue == fErrorState; }

   T fValue;
   T fErrorState;
   bool fIsChecked;
};


RStatus<FILE *> OpenFilePointer(const std::string &path) {
   return RStatus<FILE *>(fopen(path.c_str(), "r"));
}

RStatus<int> OpenFileDescriptor(const std::string &path) {
   return RStatus<int>(open(path.c_str(), O_RDONLY));
}


FILE *OpenFile(char *filename){
	pFile = fopen(filename,"r");
	if (!pFile)
	{ 
		cout << "Failed to open the file\n";	
		return nullptr;
	}
	else
	{		
	    	cout<<"Successfully opened the file\n";
	    	return pFile;
	}
}

int ReadFile(FILE* pFILE, unsigned int nBytes, void *dst)
{
   if (pFile == 0) {
	return -1;
	}

   else if(feof(pFile)) {
	cout<<endl<<__FUNCTION__<<" : File Opened but no text found in file\n"; 
	return -1;
	}
    
   else{
     
            if (pFILE)
            {
             string contents;
             fseek(pFILE, 0, SEEK_END);
             contents.resize(ftell(pFILE));
             rewind(pFILE);
             fread(&contents[0], 1, contents.size(), pFILE);
             cout<<"The content"<<endl<<contents<<endl<<"END PRINT";

             fseek(pFILE, 0, SEEK_END);
             int size_file=(ftell(pFILE));
  			    cout<<endl<<"The size of the file is "<<size_file<<endl;
             return size_file;
            
            }
    } 
}

	bool CloseFile(FILE *fp){  
	   if(fclose(fp)==0) {
              cout<<"File Handler successfully closed\n"<<endl;
		return true;
           }
	   else { 
              cout<<endl<<__FUNCTION__<<" : Error Closing File from the File Handler\n";
	   return false;
	   }
}

int Usage() {
      cout<<"Please enter filename from root directory int the command line interface itself";
      cout<<endl<<"eg. >> ./a.out /home/user/Downloads/abc.txt"<<endl;
      cout<<"Exiting the program!!"<<endl;
      return 1;
   }



void Test001() {
   bool exceptionThrown = false;
   try {
     auto fp = OpenFilePointer("no/such/file");
     // Failure occurred and fp is not checked: expect exception
   } catch (const RException&) {
      exceptionThrown = true;
   }

   // test code
   if (!exceptionThrown) {
      std::cerr << "Test001 Fail" << std::endl;
      return;
   }

   std::cout << "Test001 OK" << std::endl;
}


void Test002() {
   bool exceptionThrown = false;
   try {
     int fd = OpenFileDescriptor("no/such/file");
     
     // Failure occurred and fd is checked
   } catch (const RException&) {
      exceptionThrown = true;
   }

   // test code
   if (exceptionThrown) {
      std::cerr << "Test002 Fail" << std::endl;
      return;
   }

   std::cout << "Test002 OK" << std::endl;

}

void Test003() {
   bool exceptionThrown = false;
   try{
      auto fp = OpenFilePointer("/dev/null");
   } catch (const RException&) {
      exceptionThrown = true;
   }

   if (exceptionThrown) {
      std::cerr << "Test003 Fail" << std::endl;
      return;
   }

   std::cout << "Test003 OK" << std::endl;
}


class AnotherException : public std::runtime_error {
public:
   explicit AnotherException(const std::string &what) : std::runtime_error(what) {}
};

void Test004() {
   try {
      auto fp = OpenFilePointer("no/such/file");
      // fp wraps null because the call failed
      throw AnotherException("something else went wrong");
   } catch (const AnotherException&) {
      // This will not catch RException, so that the program crashes if fp throws
   }

   std::cout << "Test004 OK" << std::endl;
}

/*  
void Test005() {
   auto status = new RStatus<int, -1>(0);  // <-- This should not compile (heap allocated)
   RStatus<int, -1> status(0);  // <-- This, however, should compile (stack allocated)
}
*/

int main(int argc, char** argv)
{
   Test001();
   Test002();
   Test003();
   Test004();
  // Test005();
   
   if (argc<2) {
      Usage();
   }
 auto fp = OpenFile(argv[1]);

if (fp == nullptr) {
       std::cerr << "There was an error" << std::endl;
       return 1;
   }

      if((pFile = fopen(argv[1],"r")) == 0)
         {
    	    cout<<endl<<__FUNCTION__<<" : Error encountered in opening file\n"; 
    	    exit(-1);
	  }   


   int x=ReadFile(fp,100, buffer);
   
   CloseFile(fp);
return 0;	
}




