# root_exceptions

   1. I/O Error Handling in ROOT
   
The aspect of ROOT dealt with in this project is the error handling interface, specifically in the I/O components. ROOT has situations where it is expected to return an error code, but there are possibilities in which it is preferred to have ROOT throw an exception. The project  combines these two aspects of error handling by designing a class that can behave like an error code and only throws an exception if the error code is not checked.


   2. ERROR HANDLING

Errors fall into two categories: syntax and semantic errors. A syntax error occurs when you write a statement that is not valid according to the grammar of the C++ language whereas a semantic error occurs when a statement is syntactically valid, but does not do what the programmer intended. Syntax errors are almost always caught by the compiler and are usually easy to fix. On the other hand, semantic errors are not caught by the compiler, and can affect the program in the following ways: 
-They may not show up at all
-Cause the program to produce the wrong output or Corrupt data 
-Cause the program to crash.
To tackle these issues, it is necessary to handle errors.

	2.1. Return Error Codes

A return code or an error code is a numbered or alphanumeric code that is used to determine the nature of an error, and why it occurred. They are commonly found in cases when the code attempts to do something it cannot do i.e. executing something that is outside the scope of the code. They can also passed off to error handlers that determine what action to take.
By default, the command line execution should return zero when execution succeeds and non-zero value when execution fails. When execution fails, the returned non-zero value indicates the corresponding error number.

But are error codes always helpful?
Here are a few examples where they are not very effective.

If a function returns 1, is it trying to indicate an error, or is that actually a valid return value?
Functions can only return one value, so what happens when you need to return both, a function result and an error code? 
Deep call stacks could be a problem. The error would have to travel back and forth within the stack.
In sequences of code where many things can go wrong, error codes have to be checked constantly since it gets difficult to trace the origin of a particular error.
Constructors don’t have return types.

	2.2 Exceptions

Exception handling provides a mechanism to decouple handling of errors or other exceptional circumstances from the typical control flow of the code. They are able to separate the actual logic of the code and the error handling.
Exceptions are thrown at a precise location of the error. This allows more freedom to handle errors for a given situation, alleviating some of the issues that return codes cause

Also, it is always preferred to use try/catch blocks when working with files, because the state of a file can change outside a program.
Consider the following example:
if(File.Exists("file.txt"))
    File.Delete("file.txt")
The file might have been deleted by another process right after the if statement, before the Delete() call. When you try to delete it, an exception is raised.
When working with files there are also a lot more things to consider, that might not be able to catch with ifs, for example the file is on a network connection that got unavailable, access rights that change, hard disk failure etc.

   2.3 Comparing Error Coding & Exception Handling

Error Coding
PROS
Writing the code is easier as it does not have a difficult syntactical language construction.
Preferred in cases where recovery is possible.
Faster execution (in most cases) when compared with exception handling.
CONS
Easy to forget error cryptic codes.
Easy to forget to handle error codes
Error code arguments are not clear for success v/s error codes.

Exception Handling
PROS
Reporting errors from constructors is possible.
Exceptions are easily propagated from deeply nested functions.
Function signatures are simpler.
CONS
Examine all of its transitive callers when adding a ‘throw’ statement.
Runtime Penalty due to stack unwinding

When using return codes, the program is preparing itself for failure by hoping that the tests are secure enough.
But when using exceptions, the program knows that it can fail, and usually puts counterfire catch at chosen strategic position in the code. 
With exceptions you only need to handle the error if it could be a serious problem for the program. Otherwise it will be silently passed back to the caller.

   3. THE CODE
	3.1. Reading a File   

A set of 3 functions:
OpenFile()
ReadFile()
CloseFile()
perform the task of reading from a file. The OpenFile() returns the file pointer in case of successfully opening the file whereas it returns an RStatus wrapped nullptr in a case where the file was not opened. 
The ReadFile() reads the contents of the file and displays the number of bytes read. The CloseFile() takes the filehandle from OpenFile() and closes it.
   
	3.2. Class RStatus

RStatus works as a templated class that takes one template parameter: the status type (e.g. FILE *, int, bool). The class behaves like an error code and only throws an exception if the error code is not checked.

	3.3. Class RExceptions

The RException class inheriting from std::runtime_error acts as the base class for all exceptions from ROOT. The class assigns exception IDs and checks for inner exceptions. 
Basically, RException Class is a class that derives a class, that can hold the properties of both the currently handled exception (as its inner exception) and the other exception (its outer exception) making it possible to nest exceptions of arbitrary types within each other.

	3.4. Tests

The following functions test different scenarios in which the class is used:-
Test001
In a scenario where OpenFile() fails, it returns the RStatus wrapped nullptr and also the return value is ignored; it should fire an exception as soon as the block in which FileOpen() was called ends.
Test002
FileOpen() fails and its return value is checked. No exception should be fired.
Test003
FileOpen() succeeds and it's return value is not checked. No exception should be fired.
Test004
OpenFile() fails, it returns the RStatus wrapped nullptr and also the return value is ignored. But right after the FileOpen() is called, another exception concerning a different function is thrown. This should be prevented from happening.
Test005
It should be impossible to create RStatus objects on the heap. RStatus should only ever exist on the stack.
            
   4. RSTATUS CLASS

RStatus works as a templated class that takes one template parameter: the status type. The constructor defines a function: fStatus. On the execution of this function, the result: value is returned by the constructor.
If the instance of the RStatus class stores an unsuccessful state that is never checked (and only then), it should throw an exception.
The noexcept operator performs a compile-time check that returns true if an expression is declared to not throw any exceptions. In this case, the value of noexcept is given as: false. This means that if an exception is thrown, noexcept will be given as false, the program will go out of scope and the destructor RStatus will be called.

~RStatus is the one that does almost everything concerned with checking if the error has been queried or not.  
An exception is thrown if the fStatus function returns a value which gives an error: IsError(fStatus), and if the error is not checked: !fIsChecked. The uncaught exceptions are checked in order to prevent throwing an exception if the object is deconstructed in the course of stack unwinding for another exception (as mentioned in Test004). Thus if there are no uncaught exceptions: !std::uncaught_exceptions() , an exception is thrown. 

template <typename T>
class RStatus {
public:
   RStatus(const T &value) : fStatus(value) { }
   ~RStatus() noexcept(false)
   {
      if (!fIsChecked && IsError(fStatus)) {
         // Prevent from throwing if the object is deconstructed in the course of stack unwinding for another exception
         if (!std::uncaught_exceptions())
            throw RException("return value not checked");
      }
   }

In a scenario where everything goes as expected, i.e. the error has been checked then no exceptions are thrown and the value got by execution of the function fStatus is returned: return fStatus.fValue

// Use the RStatus<T> wrapper like a T
   operator const T&() {
      fIsChecked = true;
      return fStatus.fValue;
   }
private:
   T fStatus;
   bool fIsChecked = false;
};

While functions and classes are powerful and flexible tools for effective programming, in some cases, they can be inconvenient when it is expected to specify the datatype of all parameters. Template classes are instanced as the compiler stencils out a copy upon demand, with the template parameter replaced by the actual data type the user needs, and then compiles the copy.

The RStatus<int> and the RStatus<FILE *> are the specialised template classes. As seen below, their working is the same for both int and FILE *. Class template specializations are treated as completely independent classes, even though they are allocated in the same way as the templated class. This means that we can change anything and everything about our specialization class, including the way they are implemented and even the functions they makes public, just as if they were independent classes.


RStatus<int> OpenFile(const std::string &path) 
{
   return fopen(path.c_str(), "r");
}
{
   auto fp = OpenFile("path");
   
   if (fp == nullptr) {
      ...
   }
} 
RStatus<FILE *> OpenFile(const std::string &path) 
{
   return fopen(path.c_str(), "r");
}
{
   auto fp = OpenFile("path");
   
   if (fp == nullptr) {
      ...
   }
}

   5. DIFFICULTIES FACED

Preventing exceptions from being fired during the course of stack unwinding:-
As previously stated in Test004, OpenFile() fails, it returns the RStatus wrapped nullptr and also the return value is ignored. But right after the FileOpen() is called, another exception concerning a different function is thrown. This has to be prevented from happening.
In the process of Stack Unwinding, the program calls the destructors for objects allocated since the starting of any code block. Objects that were created within the block are deallocated in reverse order of their allocation.
By using std::uncaught_exceptions, the code is able to detect if the current thread has a live exception object, that is, an exception has been thrown or rethrown and not yet entered a matching catch clause, std::terminate or std::unexpected. In other words, std::uncaught_exception detects if stack unwinding is currently in progress.
It also detects how many exceptions in the current thread have been thrown or rethrown and not yet entered their matching catch clauses.

Preventing RStatus objects from being created on the heap:-
Stack is a special region of the computer's memory that stores temporary variables created by each function. The stack is a "LIFO" (last in, first out) data structure, that is managed and optimized by the CPU quite closely. Once a stack variable is freed, that region of memory becomes available for other stack variables.
The heap is a region of the computer's memory that is not managed automatically and is not as tightly managed by the CPU. It is a more free-floating region of memory.
Once memory has been allocated on the heap, the programmer is responsible for using free() to deallocate that memory when needed any more. If the programmer fails to do this, the program will suffer a memory leak.

Thus it should be impossible to create RStatus objects on the heap for ROOT. RStatus should only ever exist on the stack.
To make RStatus non-creatable on the heap, the new operator has to be overwritten.
This condition has been checked in Test005.

void Test005() {
   auto status = new RStatus<int, -1>(0);  // <-- This should not compile (heap allocated)
   RStatus<int, -1> status(0);  // <-- This, however, should compile (stack allocated)
}


   6. LOOKING AHEAD

The project is currently in the demonstrator state which means that it can run independently. 
The Next Step is to integrate the code with ROOT and make it a part of ROOT experimental and then add it to the RNTuple class.

   7. CONCLUSION

The project  combines the two aspects of error handling by designing the RStatus class that can behave like an error code and only throws an exception if the error code is not checked.

   8. References

[1] https://www.learncpp.com/
[2] https://en.cppreference.com/w/
[3] https://docs.microsoft.com/en-us/windows/win32/debug/error-handling
[4] https://www.gribblelab.org/CBootCamp/
